export class User {
  id: number;
  email: string;
  password: string;
  fullname: string;
  roles: ('admin' | 'user')[];
  gender: 'male' | 'female' | 'others';
}
